export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "PHP Laravel Course",
		price: 45000,
		onOffer: false,
		start_date: '2020-02-20',
		end_date: '2020-08-19'
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Python Django Course",
		price: 55000,
		onOffer: true,
		start_date: '2020-02-20',
		end_date: '2020-08-19'
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Java Springboot",
		price: 75000,
		onOffer: true,
		start_date: '2020-02-20',
		end_date: '2020-08-19'
	}
]