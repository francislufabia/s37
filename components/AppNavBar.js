import { Navbar, Nav } from 'react-bootstrap'
//import nextJS Link component for client-side navigation
import Link from 'next/link'

export default function AppNavBar(){
	return(
		<Navbar bg="light" expand="lg">
		  	<Link href="/">
	  			<a href="#" className = "navbar-brand">Course Booking</a>
		  	</Link>		  	
	
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto">
		    	<Link href = "/">
		    		<a href = "#" className = "nav-link"> Home </a>
		    	</Link>
		    	<Link href = "/login">
		    		<a href = "#" className = "nav-link"> Login </a>
		    	</Link>
		    	<Link href = "/register">
		    		<a href = "#" className = "nav-link"> Register </a>
		    	</Link>
		    	<Link href = "/courses">
		    		<a href = "#" className = "nav-link"> Courses </a>
		    	</Link>

		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
		)
}