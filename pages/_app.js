import {useState, useEffect} from 'react'
import '../styles/globals.css'
//import CSS Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import Container from 'react-bootstrap/Container'
import AppNavBar from '../components/AppNavBar'
import { UserProvider } from '../UserContext'
import AppHelper from '../apphelper'

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		email: null,
		isAdmin: null,
		id: null
	})

	const unSetUser = () => {
		localStorage.clear()
		setUser({
			email: null,
			isAdmin: null,
			id: null			
		})
	}

	useEffect(()=> {
		if(localStorage.getItem('token')){
			const payload = {
				headers: {Authorization: `Bearer ${AppHelper.getAccessToken()}`}
			}
			fetch(`${AppHelper.API_URL}/users/details`, payload)
			.then(res => res.json())
			.then(data => {
				setUser({
					email: data.email,
					isAdmin: data.isAdmin,
					id: data._id
				})
			})
			.catch(error => console.log(error))
		} else {
			console.log("No Authenticated User")
		}
	},[user.id])


  return (
  	<UserProvider value = {{ user, setUser, unSetUser }}>
  		<AppNavBar/>
	  	<Container>
			<Component {...pageProps} />
	  	</Container>
	</UserProvider>
  	)

}

export default MyApp
