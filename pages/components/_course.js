import { Col, Card, Button } from 'react-bootstrap'

export default function Course ({course}) {
	console.log(course)
	return (
		<Col>
			<Card style={{ width: '18rem' }}>
			  <Card.Body>
			    <Card.Title>{course.name}</Card.Title>
			    <Card.Subtitle className="mb-2 text-muted">Price: {course.price}</Card.Subtitle>
			    <Card.Text>
				    	{course.description}
			    </Card.Text>
			    <Card.Link href="#">Enroll</Card.Link>
			    <Card.Link href="#">Check Details</Card.Link>
			  </Card.Body>
			</Card>
		</Col>
	)
}