import {useState, useEffect} from 'react'
import Head from 'next/head'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import AppHelper from '../apphelper'
export default function register(){

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [vpassword, setVpassword] = useState("")
	const [disable, setDisable] = useState(true)

	useEffect(()=>{
		if((email !== "" && password !== "" && vpassword !== "") && (password === vpassword)){
			setDisable(false) //enable button
		} else {
			setDisable(true) //disable button
		}
	},[email, password, vpassword])


	const registerUser = (e) => {
		e.preventDefault()

		const payload = {
			method:'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstName: "Juan",
				lastName: "Cruz",
				email: email,
				mobileNo: "09212345687",
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}/users/register`, payload)
		.then(res => res.json())
		.then(data => {
			if(data == true){
				alert("You have successfully registered an account!")
			}else {
				alert("Oops! Something went wrong!")
			}
		})
		.catch(error => console.log(error))
	}


	return(
		<div>
			<Head>
				<title>Course Booking | Register</title>
			</Head>

			<Row className="mt-4 pt-4 justify-content-center">
				<Col md={8}>
					<Form onSubmit={(e)=> registerUser(e)}>
					  <Form.Group controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email"
					    	value={email}
					    	onChange={(e)=> setEmail(e.target.value)}
					    	/>
					    <Form.Text className="text-muted">
					      We'll never share your email with anyone else.
					    </Form.Text>
					  </Form.Group>

					  <Form.Group controlId="formBasicPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value={password}
					    	onChange={(e)=> setPassword(e.target.value)}
					    />
					  </Form.Group>
					  
					  <Form.Group controlId="formBasicVPassword">
					    <Form.Label>Verify Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Verify Password"
					    	value={vpassword}
					    	onChange={(e)=> setVpassword(e.target.value)}
					    />
					  </Form.Group>

					  <Button variant="primary" type="submit" disabled={disable}>
					    Sign up
					  </Button>
					</Form>	
				</Col>
			</Row>
		</div>
		)
}

/*
	Activity Instructions:

	1. Create a state that will handle the getter and setter for the ff:
		email, password and verify password
	2. Bind the states to their respective input fields
	3. Use effect hook that will check the values for email, password and verify password, create the ff condition:
		a. If the email, password and verify password is not empty AND password value is equal to verify password value, then enable the Sign up button
		b. If letter a is not met, then disable the Sign up button
	
	Deadline: 8:50PM

*/