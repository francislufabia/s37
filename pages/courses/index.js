import Row from 'react-bootstrap/Row'

//relative imports
import coursesData from './../../data/courses'
import Course from './../components/_course'

export default function index(){

	console.log('Courses Data: ', coursesData)

	//create multiple course components corresponding to the data inside the coursesData
	const courses = coursesData.map(course => {
		console.log(course)
		return (
			<Course key = {course.id} course={course}/>
		)
	})

	return(
		<>
			<h1 className="my-5 bg-primary text-white text-center"> Courses </h1>
			<Row>
				{courses}
			</Row>
		</>
		)
}