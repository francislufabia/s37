import {useState, useEffect} from 'react'
import Head from 'next/head'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
//relative imports
import courses from './../../data/courses'

export default function addcourse(){
	const [cid, setCid] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [startDate, setStartDate] = useState("")
	const [endDate, setEndDate] =  useState("")
	const [disable, setDisable] = useState(true)
	console.log(courses)


	useEffect(()=>{
		if(cid !== "" && name !== "" && description !== "" && price !== ""){
			setDisable(false)
		} else {
			setDisable(true)
		}
	}, [cid, name, description, price])

	const addCourseHandler = e => {
		e.preventDefault()
		const courseData = {
			id: cid, 
			name: name,
			description: description,
			onOffer: false,
			price: price,
			start_date: startDate,
			end_date: endDate
		}
		console.log(courseData)

		courses.push(courseData)
		console.log('Updated Courses: ', courses)
	}

	return(
		<div>
			<Head>
				<title>Add Course</title>
			</Head>

			<Row className="mt-4 pt-4 justify-content-center">
				<Col md={8}>
					<Form onSubmit={addCourseHandler}>
					  <Form.Group controlId="courseID">
					    <Form.Label>Course ID</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course ID" 
					    	value={cid}
					    	onChange={(e)=> setCid(e.target.value)}
					    	/>
					  </Form.Group>

					  <Form.Group controlId="courseName">
					    <Form.Label>Course Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course Name"
					    	value={name}
					    	onChange={(e)=> setName(e.target.value)}
					    />
					  </Form.Group>

					  <Form.Group controlId="courseDescription">
					    <Form.Label>Course Description</Form.Label>
					     <Form.Control 
					     	as="textarea" 
					     	rows={3} 
					     	placeholder="Enter Course Description"
					     	value={description}
					     	onChange={(e)=> setDescription(e.target.value)}
					     />
					  </Form.Group>

					  <Form.Group controlId="coursePrice">
					    <Form.Label>Course Price</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course Price" 
					    	value={price}
					     	onChange={(e)=> setPrice(e.target.value)}
					    />
					  </Form.Group>

					  <Form.Group controlId="courseStartDate">
					    <Form.Label>Start Date</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Start Date" 
					    	value={startDate}
					     	onChange={(e)=> setStartDate(e.target.value)}
					    />
					  </Form.Group>

					  <Form.Group controlId="courseEndDate">
					    <Form.Label>End Date</Form.Label>
					    <Form.Control 
						    type="text" 
						    placeholder="Enter End Date" 
						    value={endDate}
					     	onChange={(e)=> setEndDate(e.target.value)}
						   />
					  </Form.Group>
					
					  <Button variant="primary" type="submit" disabled={disable}>
					    Add Course
					  </Button>
					</Form>
				</Col>
			</Row>

		</div>

		)
}


/*
	Activity instructions:

	1. Create the states for the ff values:
		course id, course name, course description, course price, start date and end date
	2. Bind the states to their necessary input fields
	3. Use effect hook to check the values for the courses and create the ff condition:
		a. if course id, name, description and price is not empty, enable the Add course button
		b. if letter a is not met, disable the button

	9:25pm



*/